Une carte représentant le Mur de l'Evol', accompagnée d'aides de jeu, d'intérêts scénaristiques et d'un scénario en une page.

Toutes les images, en dehors des cartes, viennent de Pixabay.com.
